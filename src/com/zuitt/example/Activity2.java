package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity2 {

    public static void main(String[] args) {

        Scanner scanObj = new Scanner(System.in);

    // A.
        // 2. Declaring the array for containing the first 5 prime numbers as integers.
        int[] primeNumbersArr = new int[5];

        // 3. Assigning the first 5 prime numbers to their respective indices.
        primeNumbersArr[0] = 2;
        primeNumbersArr[1] = 3;
        primeNumbersArr[2] = 5;
        primeNumbersArr[3] = 7;
        primeNumbersArr[4] = 11;
        // System.out.println(Arrays.toString(primeNumbersArr));

        // 4. Output specific elements of the array in the terminal by specifying the index of the target element.
        System.out.println("The first prime number is: " + primeNumbersArr[0]);

    // B.
        // 1. Creat an ArrayList of String data-type elements using generics and add 4 elements to it.
        ArrayList<String> friends = new ArrayList<>(Arrays.asList());
        friends.add(0, "John");
        friends.add(1, "Jane");
        friends.add(2, "Chloe");
        friends.add(3, "Zoe");

        // 2. Output the contents of the ArrayList concatenated with a string message in the console.
        System.out.println("My friends are: " + friends);

        // C. HashMap
        // Using generics, create a HashMap of keys with data type String and values with data type integer.
        HashMap<String, Integer> inventory = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        // Output the contents of the HashMap concatenated with a string message in the console.
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
