package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        // Java Operators.
        // Arithmetic -> +, -, *, /, %
        // Comparison -> >, <, >=, <=, ==, !=
        // Logical -> &&, ||, !
        // Assignment -> =

        // Selection Control Structure in Java
        // if else
        // Syntax:
            /*if(condition) {
                // Code block
            }
            else {
                // Code Block
            }*/

        int num = 36;
        if (num % 5 == 0) {
            System.out.println(num + " is divisible by 5.");
        } else {
            System.out.println(num + " is not divisible by 5");
        }

        int x = 0;
        int y = 1;

        if (x / y == 0 || y != 0 && x != 0) {
            System.out.println("Result is: " + x / y);
        } else {
            System.out.println("This will only run because of short circuiting.");
        }

        // Ternary Operator
        int number = 24;
        Boolean result = (number > 0) ? true : false;
        System.out.println(result);

        // Switch Cases
//        Scanner numberScanner = new Scanner(System.in);
//        int directionValue = numberScanner;
//
//        switch (directionValue) {
//                case 1:
//                    System.out.println("North");
//                case 2:
//                    System.out.println("East");
//                case 3:
//                    System.out.println("South");
//                case 4:
//                    System.out.println("West");
//    }

    }
}
