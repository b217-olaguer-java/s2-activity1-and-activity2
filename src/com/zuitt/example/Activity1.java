package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {

        Scanner scanObj = new Scanner(System.in);
        System.out.println("Input year to be checked if it is a leap year.");
        int year = scanObj.nextInt();

        if(year == 0){
            System.out.println(year + " is not a valid year.");
        }
        else if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            System.out.println(year + " is a leap year.");
        }
        else {
            System.out.println(year + " is NOT a leap year.");
        }
   }
}
