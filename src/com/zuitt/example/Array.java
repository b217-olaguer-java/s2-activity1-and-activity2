package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    // Java Collection
        // Are single unit of objects
        // Useful for manipulating relevant pieces of data that can be used in different situations
    public static void main(String[] args) {
        // Arrays :
            // - are containers of values of the SAME data type given a predefined amount of values.
            // - are more rigid, once the size and data type are defined, they can no longer be changed.

            // Syntax: Array Declaration
                // dataType[] identifier = new  dataType[numberOfElements]
                // the values of the array is initialized to 0 or null.
//        int[] intArray = new int[5];
//        intArray[0] = 200;
//        intArray[1] = 3;
//        intArray[2] = 25;
////        intArray[3] = 50;
//        intArray[4] = 98;
//
//        System.out.println(intArray[2]);
//        // Array memory code print.
//        System.out.println(intArray);
//
//        // to print Array
//        System.out.println(Arrays.toString(intArray));
//
//        boolean[] bArr = new boolean[2];
//        bArr[0] = true;
//
//        System.out.println(Arrays.toString(bArr));
//
//        // Declaring Arrays with Initialization
//        // Syntax:
//            // dataType[] identifier = {element}
//
//        String[] names = {"John", "Jane", "Joe", "", null, "Jimmy" };
//
//        System.out.println(Arrays.toString(names));
//        System.out.println(names[2]);
//        System.out.println(names[3]);
//        System.out.println(names[4]);
//        System.out.println(names[5]);
//
//        Arrays.sort(intArray);
//        System.out.println(Arrays.toString(intArray));
//
//        // Multidimensional Arrays
//            // Syntax:
//                // dataType[][] identifier = new dataType[rowLength][colLength]
//
//        String[][] classroom = new String[3][3];
//
//        // First Row
//        classroom[0][0] = "Athos";
//        classroom[0][1] = "Porthos";
//        classroom[0][2] = "Aramis";
//
//        // Second Row
//        classroom[1][0] = "Brandon";
//        classroom[1][1] = "Jane";
//        classroom[1][2] = "Bogart";
//
//        // Third Row
//        classroom [2][0] = "Jack";
//        classroom [2][1] = "Jill";
//        classroom [2][2] = "Hill";
//
//        System.out.println(Arrays.toString(classroom));
//        System.out.println(Arrays.deepToString(classroom));

        // ArrayLists
            // are resizeable arrays, wherein elements can be added ro removed whenever it is needed.
            // Syntax:
                // ArrayList<dataType> identifier = new ArrayList<dataType>()

            // Declare an arrayList
            // ArrayList<String> students = new ArrayList<>();

            // Adding elements to ArrayList is done by the add() function
//            students.add("John");
//            students.add("Bogart");
//            System.out.println(students);

            // Declare an ArrayList with values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("John", "Paul"));
        System.out.println(students);

        System.out.println(students.get(1));

        // Updating an element
        students.set(1, "Bogart");
        System.out.println(students.get(1));

        // Add an element
        students.add(1, "Carpio");
        System.out.println(students);

        // Remove a specific element
        students.remove(2);
        System.out.println(students);

        // Removing all elements
        students.clear();
        System.out.println(students);

        // Getting the number of elements in an ArrayList
        System.out.println(students.size());

        ArrayList<Integer> numbs = new ArrayList<>(Arrays.asList(1, 2));
        System.out.println(numbs);

        // HashMaps
            // Collection of data in "key-value pairs"
            // in Java, "keys" are also referred by the "fields"
            // wherein the values are accessed by the "fields"

        // Syntax:
            // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();
        // Declaring HashMaps
        HashMap<String, String> jobPosition = new HashMap<>();
        System.out.println(jobPosition);

        jobPosition.put("Developer", "Magic");
        System.out.println(jobPosition);

        jobPosition.put("Student", "Alice");
        jobPosition.put("Student", "Natsu");
        System.out.println(jobPosition);

    // Creating HashMap with initialization
        HashMap<String, String> jobPosition2 = new HashMap<>() {
            {
                put("Student", "Alice");
                put("Developer", "Magic");
            }
        };
        System.out.println(jobPosition2);

        // Accessing elements in HashMaps
        System.out.println(jobPosition.get("Developer"));

        // Updating an element
        jobPosition.replace("Student", "Jacob");
        System.out.println(jobPosition);

        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());

        jobPosition.remove("Student");
        System.out.println(jobPosition);

        jobPosition.clear();
        System.out.println(jobPosition);
    }




}
